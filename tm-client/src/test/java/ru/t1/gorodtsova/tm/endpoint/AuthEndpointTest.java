package ru.t1.gorodtsova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.gorodtsova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.gorodtsova.tm.dto.request.user.UserLoginRequest;
import ru.t1.gorodtsova.tm.dto.request.user.UserLogoutRequest;
import ru.t1.gorodtsova.tm.dto.response.user.UserLoginResponse;
import ru.t1.gorodtsova.tm.dto.response.user.UserLogoutResponse;
import ru.t1.gorodtsova.tm.marker.IntegrationCategory;

@Category(IntegrationCategory.class)
public final class AuthEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Test
    public void login() {
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
        Assert.assertTrue(userLoginResponse.getSuccess());

        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("", "test"))
        );
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("test", ""))
        );
        Assert.assertThrows(
                Exception.class,
                () -> authEndpoint.login(new UserLoginRequest("wrongLogin", "wrongPassword"))
        );
    }

    @Test
    public void logout() {
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("test", "test")
        );

        @NotNull final UserLogoutResponse userLogoutResponse = authEndpoint.logout(
                new UserLogoutRequest(userLoginResponse.getToken())
        );
        Assert.assertNotNull(userLogoutResponse);
        Assert.assertTrue(userLogoutResponse.getSuccess());

        Assert.assertThrows(Exception.class, () -> authEndpoint.logout(new UserLogoutRequest()));
        Assert.assertThrows(Exception.class, () -> authEndpoint.logout(new UserLogoutRequest(null)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.logout(new UserLogoutRequest("wrongToken")));
    }

}
