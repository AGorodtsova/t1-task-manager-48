package ru.t1.gorodtsova.tm.dto.request.domain;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class DataJsonSaveFasterXmlRequest extends AbstractUserRequest {

    public DataJsonSaveFasterXmlRequest(@Nullable final String token) {
        super(token);
    }

}
