package ru.t1.gorodtsova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.gorodtsova.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.gorodtsova.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.gorodtsova.tm.api.service.IConnectionService;
import ru.t1.gorodtsova.tm.api.service.IPropertyService;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.repository.dto.SessionDtoRepository;
import ru.t1.gorodtsova.tm.repository.dto.UserDtoRepository;
import ru.t1.gorodtsova.tm.service.ConnectionService;
import ru.t1.gorodtsova.tm.service.PropertyService;

import javax.persistence.EntityManager;

import static ru.t1.gorodtsova.tm.constant.SessionTestData.*;
import static ru.t1.gorodtsova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    private ISessionDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionDtoRepository(entityManager);
    }

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = new UserDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1);
            repository.add(USER2);
            repository.add(ADMIN1);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repositoryUser = new UserDtoRepository(entityManager);
            @NotNull final ISessionDtoRepository repositorySession = new SessionDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repositorySession.removeAll();
            repositoryUser.removeAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void add() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_SESSION1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1_SESSION1, repository.findAll().get(0));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void addByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1.getId(), USER1_SESSION1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1_SESSION1, repository.findAll().get(0));
            Assert.assertEquals(USER1.getId(), repository.findAll().get(0).getUserId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void addAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_SESSION_LIST);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1_SESSION_LIST, repository.findAll());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAllByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(SESSION_LIST);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1_SESSION_LIST, repository.findAll(USER1.getId()));
            Assert.assertNotEquals(USER1_SESSION_LIST, repository.findAll(USER2.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findOneByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_SESSION1);
            repository.add(USER2_SESSION1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1_SESSION1, repository.findOneById(USER1.getId(), USER1_SESSION1.getId()));
            Assert.assertNotEquals(USER2_SESSION1, repository.findOneById(USER1.getId(), USER2_SESSION1.getId()));
            Assert.assertNull(repository.findOneById(USER1.getId(), USER2_SESSION1.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_SESSION1);
            repository.add(USER2_SESSION1);
            repository.removeOneById(USER1.getId(), USER1_SESSION1.getId());
            entityManager.getTransaction().commit();
            Assert.assertFalse(repository.findAll().contains(USER1_SESSION1));
            Assert.assertTrue(repository.findAll().contains(USER2_SESSION1));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_SESSION1);
            repository.add(USER2_SESSION1);
            repository.removeOne(USER1.getId(), USER1_SESSION1);
            entityManager.getTransaction().commit();
            Assert.assertFalse(repository.findAll().contains(USER1_SESSION1));
            Assert.assertTrue(repository.findAll().contains(USER2_SESSION1));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeAllByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_SESSION_LIST);
            repository.add(USER2_SESSION_LIST);
            repository.removeAll(USER1.getId());
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER2_SESSION_LIST, repository.findAll());
            Assert.assertNotEquals(USER1_SESSION_LIST, repository.findAll());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void existsByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_SESSION1);
            entityManager.getTransaction().commit();
            Assert.assertTrue(repository.existsById(USER1_SESSION1.getId()));
            Assert.assertFalse(repository.existsById(USER2_SESSION1.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
